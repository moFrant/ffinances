from core.views import BaseView

class MainPage(BaseView):
    template_name       = 'base.html'
    http_method_names   = ['get']
