# -*- coding: utf-8 -*-

from django.db import models
from django.contrib import admin

class Model(models.Model):
    date_created  = models.DateTimeField(auto_now_add=True, verbose_name=u'дата создания')
    date_modified = models.DateTimeField(auto_now=True, verbose_name=u'дата изменения')

    class Meta:
        abstract = True
        
class ModelAdmin(admin.ModelAdmin):
    readonly_fields=('date_created', 'date_modified')
