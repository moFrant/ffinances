from django.shortcuts       import redirect
from django.conf            import settings
from django.core.exceptions import PermissionDenied


def CheckUser(function):
    def run(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return redirect(settings.USER_SYS_LOGIN['login_adres'] + '?way=' + str(self.request.path_info))
        if not self.request.user.is_active:
            return redirect(settings.USER_SYS_LOGIN['login_adres'] + '?way=' + str(self.request.path_info))
            
        return function(self, self.request, *args, **kwargs)
        
    return run
