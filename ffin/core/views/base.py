from django.views.generic.base  import TemplateView

class BaseView(TemplateView):
    template_name       = None
    http_method_names   = []
    form                = False
    
    def get_context_data(self, **kwargs):
        context = super(BaseView, self).get_context_data(**kwargs)
        
        if "message" in self.request.session:
            context["message"] = self.request.session["message"]
            del self.request.session["message"]
            
        return context
