from django.conf.urls import url

from .views import LoginFormView, logout

urlpatterns = [
     url('^login/$', LoginFormView.as_view()),
     url('^logout/$', logout),
]
