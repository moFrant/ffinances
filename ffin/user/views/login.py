# -*- coding: utf8 -*-

from django.views.generic.edit import FormView
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login


class LoginFormView(FormView):
    form_class = AuthenticationForm

    # Аналогично регистрации, только используем шаблон аутентификации.
    template_name = "login.htm"

    # В случае успеха перенаправим на главную.
    success_url = "/"

    def get_success_url(self):
        way = self.request.GET['way']
        LoginFormView.success_url = way
        return super(LoginFormView, self).get_success_url()

    def form_valid(self, form):
        # Получаем объект пользователя на основе введённых в форму данных.
        self.user = form.get_user()

        # Выполняем аутентификацию пользователя.
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)
