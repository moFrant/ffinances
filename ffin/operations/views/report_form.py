from django.shortcuts           import redirect
from django.utils.timezone      import utc

from core.decorators            import CheckUser
from core.views                 import BaseView

from ..models                   import Operation
from ..forms                    import RepotrForm


class RepotrOperation(BaseView):
    template_name       = 'operation_form.html'
    http_method_names   = ['get', 'post']
    form                = False
    operations          = False
    
    @CheckUser
    def get(self, request, *args, **kwargs):
        self.request = request
        
        return super(RepotrOperation, self).get(request, *args, **kwargs)
        
    @CheckUser
    def post(self, request, *args, **kwargs):
        self.request = request
        
        self.form = RepotrForm(request.POST)
            
        if not self.form.is_valid():
            return super(RepotrOperation, self).get(request, *args, **kwargs)
                
        self.operations = Operation.objects.filter(
                date__gte = self.form.cleaned_data["date_start"].replace(tzinfo=utc),
            ).filter(
                date__lte = self.form.cleaned_data["date_end"].replace(tzinfo=utc),
            ).order_by(
                "date"
            )
            
        return super(RepotrOperation, self).get(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        context = super(RepotrOperation, self).get_context_data(**kwargs)
        
        if not self.operations:
            if self.form == False:
                form = RepotrForm()
            else:
                form = self.form
        else:
            form = False
        
        context.update({
            'form':         form,
            'button_name':  u'Сформировать',
            'operations':   self.operations,
            })
            
        return context
