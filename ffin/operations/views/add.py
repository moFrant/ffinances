from django.shortcuts           import redirect
from django                     import forms
from django.utils.timezone      import utc

from core.decorators            import CheckUser
from core.views                 import BaseView
from article.models             import Article

from ..models                   import Operation
from ..forms                    import OperationForm

from datetime                   import datetime


class AddOperation(BaseView):
    template_name       = 'operation_form.html'
    http_method_names   = ['get', 'post']
    form                = False
    
    @CheckUser
    def get(self, request, *args, **kwargs):
        self.request = request
        
        return super(AddOperation, self).get(request, *args, **kwargs)
        
    @CheckUser
    def post(self, request, *args, **kwargs):
        self.request = request
        
        self.form = OperationForm(request.POST)
            
        if not self.form.is_valid():
            return super(AddOperation, self).get(request, *args, **kwargs)
                
        operation = self.form.save()
        operation.user = request.user
        operation.save()
        
        return redirect('/')


    def get_context_data(self, **kwargs):
        context = super(AddOperation, self).get_context_data(**kwargs)
        
        if self.form == False:
            OperationForm.base_fields["article"] = forms.ModelChoiceField(queryset=Article.objects.filter(date_start__lte = datetime.utcnow().replace(tzinfo=utc), date_end__gt = datetime.utcnow().replace(tzinfo=utc)))
            form = OperationForm()
        else:
            form = self.form
        
        context.update({
            'form':         form,
            'button_name':  u'Добавить',
            })
            
        return context
