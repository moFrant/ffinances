from django.forms   import ModelForm
from django         import forms

from .models        import Operation

class OperationForm(ModelForm):
    class Meta:
        model = Operation
        fields = [
            'date',
            'article',
            'summa',
            ]
            

class RepotrForm(forms.Form):
    date_start = forms.DateTimeField(
        label = 'Дата начала',
        )
    date_end = forms.DateTimeField(
        label = 'Дата конца',
        )
