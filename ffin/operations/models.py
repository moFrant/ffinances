from django.db              import models
from django.conf            import settings

from core.models            import Model

from article.models         import Article


# Create your models here.

class Operation(Model):
    date = models.DateTimeField(
        verbose_name = u"дата операции"
        )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name = u"кто совершил операцию",
        blank = True,
        null = True,
        )
    summa = models.FloatField(
        verbose_name = u"сумма операции",
        )
    article = models.ForeignKey(
        Article,
        verbose_name = u"статья",
        )
    
    
#    def __str__(self):
#        return str(self.date) + ': ' + self.article + ' -> ' + str(self.summa)
