from django.conf.urls import url

from .views import AddOperation, RepotrOperation

urlpatterns = [
     url('^add/$', AddOperation.as_view()),
     url('^report/$', RepotrOperation.as_view()),
]
