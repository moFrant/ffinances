from django.conf.urls import url

from .views import AddArticle, DelArticle, ArticleList

urlpatterns = [
     url('list/(?P<param>\w+)?$', ArticleList.as_view()),
     url('add/$', AddArticle.as_view()),
     url('edit/(?P<pk>[0-9]+)/$', AddArticle.as_view()),
     url('delete/(?P<pk>[0-9]+)/$', DelArticle.as_view()),
]
