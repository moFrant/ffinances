from django.db              import models
from django.utils.timezone  import utc

from core.models            import Model
from core.choices           import TYPE, get_string_name

from datetime               import datetime

# Create your models here.

class Article(Model):
    date_start  = models.DateTimeField(
        verbose_name    = u"дата начала"
        )
    date_end    = models.DateTimeField(
        verbose_name    = u"дата завершения",
        blank           = True,
        null            = True,
        )
    name        = models.CharField(
        max_length      = 50,
        verbose_name    = u"название"
        )
    type        = models.CharField(
        max_length      = 50,
        verbose_name    = u"тип",
        choices         = TYPE
        )
        
        
    def __str__(self):
        return str(get_string_name(TYPE, self.type)) + ' - ' + self.name
        
    
#    def check(self):
#        if self.date_start <= datetime.utcnow().replace(tzinfo=utc) and self.date_end > datetime.utcnow().replace(tzinfo=utc):
#            return True
#        else:
#            return False
