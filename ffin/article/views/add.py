from django.shortcuts           import redirect

from core.decorators            import CheckUser
from core.views                 import BaseView

from ..models                   import Article
from ..forms                    import ArticleForm


class AddArticle(BaseView):
    template_name       = 'article_form.html'
    http_method_names   = ['get', 'post']
    form                = False
    
    @CheckUser
    def get(self, request, *args, **kwargs):
        self.request = request
        
        return super(AddArticle, self).get(request, *args, **kwargs)
        
    @CheckUser
    def post(self, request, *args, **kwargs):
        self.request = request
        
        if "pk" in kwargs:
            try:
                article = Article.objects.get(
                    pk = int(kwargs["pk"])
                    )
            except:
                request.session["message"] = u"Вы пытаетесь отредактировать несуществующую статью."
                return redirect("/article/list/")
                
            self.form = ArticleForm(
                request.POST,
                instance=article
                )
                
        else:
            self.form = ArticleForm(request.POST)
            
        if not self.form.is_valid():
            return super(AddArticle, self).get(request, *args, **kwargs)
                
        self.form.save()
        
        return redirect("/article/list/")


    def get_context_data(self, **kwargs):
        context = super(AddArticle, self).get_context_data(**kwargs)
        
        if not 'pk' in kwargs:
            if self.form == False:
                form = ArticleForm()
            else:
                form = self.form
            context["button_name"] = u'Добавить',
        else:
            try:
                article = Article.objects.get(
                    pk = int(kwargs["pk"])
                    )
            except:
                self.request.session["message"] = u"Вы пытаетесь отредактировать несуществующую статью."
                return redirect("/article/list/")
                
            form = ArticleForm(
                instance=article
                )
                
            context["button_name"] = u'Изменить'
        
        context.update({
            'form':         form,
            })
            
        return context
