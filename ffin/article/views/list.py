from django.utils.timezone      import utc

from core.choices               import TYPE
from core.decorators            import CheckUser
from core.views                 import BaseView

from datetime                   import datetime

from ..models                   import Article


class ArticleList(BaseView):
    template_name       = 'article_list.html'
    http_method_names   = ['get',]
    
    @CheckUser
    def get(self, request, *args, **kwargs):
        self.request = request
        
        return super(ArticleList, self).get(request, *args, **kwargs)
        
        
    def get_context_data(self, **kwargs):
        context = super(ArticleList, self).get_context_data(**kwargs)
        
        if "param" in kwargs:
            if kwargs["param"] == "all":
                articles = Article.objects.all()
            elif kwargs["param"] == "active":
                articles = Article.objects.filter(
                    date_start__lte = datetime.utcnow().replace(tzinfo=utc),
                    date_end__gt    = datetime.utcnow().replace(tzinfo=utc)
                    )
            elif kwargs["param"] == "income" or kwargs["param"] == "consumption":
                articles = Article.objects.filter(
                    type = kwargs["param"]
                    )
            else:
                articles = Article.objects.all()
        else:
            articles = Article.objects.all()
            
        context.update({
            "articles": articles,
            "filtr": TYPE,
            })
            
        return context
