from django.shortcuts           import redirect

from core.decorators            import CheckUser
from core.views                 import BaseView

from ..models                   import Article


class DelArticle(BaseView):
    template_name       = 'article_form.html'
    http_method_names   = ['get',]
    
    @CheckUser
    def get(self, request, *args, **kwargs):
        try:
            article = Article.objects.get(
                pk = int(kwargs['pk'])
                )
        except:
            request.session["message"] = u"Вы пытаетесь удалить несуществующую статью."
            return redirect("/article/list/")
            
        article.delete()
            
        return redirect("/article/list/")
